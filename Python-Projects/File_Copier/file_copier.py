import cv2
import os
import shutil

print("Copying directory....\n\n")
#Only thing you would need to change each time is the directories
src = "C:/Users/pokem/aftf/Python-Projects/File_Copier/atec_data"
dest = "C:/Users/pokem/aftf/Python-Projects/File_Copier/atec_data_goal"
shutil.copytree(src, dest)

#This method seems to work on some cases
#for root, dirs, files in os.walk("atec_data", topdown=False):
#   for name in dirs:
#      print(os.path.join(root, dirs))
#      print('-------')
#      print('Copying file over\n')
#      shutil.copy(os.path.join(root, dirs), 'atec_data_goal')

#When looking at the data backup files look and see if all the files are .pngs
#If not try to implements .endswith('.png')
for root, dirs, files in os.walk("atec_data_goal", topdown=False):
   for name in files:
      print(os.path.join(root, name))
      print('-------')
      print('Converting image\n')
      img = cv2.imread(os.path.join(root, name))
      cv2.imwrite(os.path.join(root, name)[:-3] + 'jpg', img, [cv2.IMWRITE_JPEG_QUALITY, 10])

print('------------\nDeleting .pngs\n------------')

for root, dirs, files in os.walk("atec_data_goal", topdown=False):
   for name in files:
       if name.endswith('.png'):
           os.remove(os.path.join(root, name))

print('\nFinished....\n')



        