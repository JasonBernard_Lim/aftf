import cv2
import numpy as np
import matplotlib.pyplot as plt

#This function is to canny the image (aka graying the image and giving out the edges)
def canny(image):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    #res = cv2.resize(blur, (700, 500)) (this is just for resizing the image)
    canny = cv2.Canny(blur, 50, 150)
    return canny

#This displays the lines image with the highlighted line
def display_lines(image, lines):
    line_image = np.zeros_like(image)
    if lines is not None:
        for x1, y1, x2, y2 in lines:
            #params are the image, coordinates of the image space where we want the line, the color of the lines (b, g, r), thickness of the line 
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
    return line_image
    

#This gives us the region of image based on the info from the canny function
def region_of_interest(image):
    height = image.shape[0]
    polygons = np.array([
    [(200, height ), (1100, height), (550, 250)]
    ])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)   
    masked_image = cv2.bitwise_and(image, mask)
    return masked_image

#This is for the video test case
def make_coordinates(image, line_parameters):
    slope, intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1*(3/5))
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return np.array([x1, y1, x2, y2])

def average_slope_intercept(image, lines):
    left_fit = []
    right_fit = []
    for line in lines: 
        x1, y1, x2, y2 = line.reshape(4)
        parameters = np.polyfit((x1, x2), (y1, y2), 1)
        slope = parameters[0]
        intercept = parameters[1]
        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))
    left_fit_average = np.average(left_fit, axis=0)
    right_fit_average = np.average(right_fit, axis=0)
    left_line = make_coordinates(image, left_fit_average)
    right_line = make_coordinates(image, right_fit_average)
    return np.array([left_line, right_line])


#######This is for the just the picture
# image = cv2.imread('test_image.jpg')
# lane_image = np.copy(image)
# canny_image = canny(lane_image)
# cropped_image = region_of_interest(canny_image)
# #The HoughLinesP() function takes the region of interest and finds the lane-edge lines
# #HoughLinesP(image, rho, theta, intersections, place-holder array, length of the line in pixels that you will accept, max line space between lines that you will allow to be one line)
# lines = cv2.HoughLinesP(cropped_image, 2, np.pi/180, 100, np.array([]), minLineLength=40, maxLineGap=5)

# averagedLines = average_slope_intercept(lane_image, lines)
# line_image = display_lines(lane_image, averagedLines)
# #addWighted basically is the blend function that I used during the Octave class
# #it basically combines the 2 images
# combo_image = cv2.addWeighted(lane_image, 0.8, line_image, 1, 1)
# cv2.imshow("result", combo_image)

# cv2.waitKey(0)

#This is to see the plot for the image in order to determine the region of interest
#plt.imshow("result", canny)
#plt.waitKey(0)

#######This is for the video test case
cap = cv2.VideoCapture("test2.mp4")
while(cap.isOpened()):
    _, frame = cap.read()
    canny_image = canny(frame)
    cropped_image = region_of_interest(canny_image)
    lines = cv2.HoughLinesP(cropped_image, 2, np.pi/180, 100, np.array([]), minLineLength=40, maxLineGap=5)
    averagedLines = average_slope_intercept(frame, lines)
    line_image = display_lines(frame, averagedLines)
    combo_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)
    cv2.imshow("result", combo_image)
    if cv2.waitKey(1) == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()

